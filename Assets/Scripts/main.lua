
layers = {}
context = {}

base_path = nil

function LoadEntity(layer, name)
    local s = LoadSprite(name)
    local script_path = base_path.."Assets/Scripts/"..name..".lua"
    local f = loadfile(script_path)
    local callbacks = f()
    local e = {
        name=name,
        x=0,
        y=0,
        scalex=1.0,
        scaley=1.0,
        texture=s,
        OnStart=callbacks.OnStart,
        OnUpdate=callbacks.OnUpdate,
    }

    local top_layer = table.maxn(layers)
    if top_layer < layer then
        local current_layer = top_layer
        while current_layer < layer do
            current_layer = current_layer + 1
            table.insert(layers, current_layer, {})
        end
    end
    table.insert(layers[layer], e)
    e.OnStart(e)
end

function UpdateEntities()
    for i, layer in ipairs(layers) do
        for x, entity in ipairs(layer) do
            layers[i][x] = entity.OnUpdate(entity)
        end
    end

end

function Init(base)
    base_path = base
    LoadEntity(1, "batman")

    LoadEntity(2, "tomato")
    LoadEntity(3, "eagle")
    context.last_tick = GetTicks()
end

function Update()
    local now = GetTicks()
    if ( now >= context.last_tick + 17 ) then
        UpdateEntities()
        context.last_tick = now
    end
    return layers
end

