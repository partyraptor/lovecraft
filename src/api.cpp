#include <stdio.h>
#include "api.h"
#include <game.h>
#include <SDL/SDL_timer.h>


LUA_FUNCTION(LoadSprite){
        int32_t argc = lua_gettop(L);
        if(argc == 1){
                void* r = load_sprite(lua_tostring(L, 1));
                lua_pushlightuserdata(L, r);
                return 1;
        }
        return 0;
}

LUA_FUNCTION(GetTicks){
        uint32_t ticks = SDL_GetTicks();
        //printf("Ticks: %d", ticks);
        lua_pushinteger(L, ticks);
        return 1;
}

void api_init(lua_State *L){
        lua_register(L, "LoadSprite", LoadSprite);
        lua_register(L, "GetTicks", GetTicks);
}
