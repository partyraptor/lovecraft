#include <inttypes.h>

#include <stdio.h>
#include <stdlib.h>


#include <engine.h>
#include <api.h>

#include <queue.h>
#include <SDL_render.h>
#include <SDL_events.h>
#include <SDL_image.h>

extern "C" {
#include <lualib.h>
#include <lauxlib.h>
}

typedef struct engine_context engine_context;

global struct engine_context* c;

internal
void
engine_render(engine_context *context)
{
  SDL_RenderClear(context->renderer);
  GameUpdateAndRender(context);
  SDL_RenderPresent(context->renderer);
}

internal
void
engine_resize_window(engine_context *context) {
  SDL_GetWindowSize(context->window, &context->window_width, &context->window_height);
}

internal
int8_t
engine_handle_event(engine_context *context, SDL_Event* event)
{
  uint8_t should_exit = 0;
  switch(event->type)
  {
    case SDL_WINDOWEVENT_RESIZED:
    {
      engine_resize_window(context);
    } break;
    case SDL_QUIT:
    {
      should_exit=1;
    } break;
    case SDL_WINDOWEVENT_EXPOSED:
    {
      engine_resize_window(context);
    } break;
    default: {
    }
          break;
  };
  return(should_exit);
}

internal
void
engine_main_loop(engine_context *context)
{
  SDL_Event event;
  for(;;)
  {
    if(SDL_PollEvent(&event))
    {
      if(engine_handle_event(context, &event))
      {
        break;
      }
    }
    engine_render(context);
  }
}

internal
void
engine_create_window(engine_context * context)
{

  context->window = SDL_CreateWindow(
                "Game Engine",
                SDL_WINDOWPOS_UNDEFINED,
                SDL_WINDOWPOS_UNDEFINED,
                800,
                600,
                SDL_WINDOW_RESIZABLE);
  if(context->window)
  {
    context->renderer = SDL_CreateRenderer(context->window, -1, SDL_RENDERER_ACCELERATED);
    if(!context->renderer)
    {
      fprintf(stderr, "Couldn't create renderer: %s\n", SDL_GetError());
    }
    else {
      SDL_SetRenderDrawBlendMode(context->renderer, SDL_BLENDMODE_BLEND);
      engine_resize_window(context);
    }
  }
  else
  {
    fprintf(stderr, "Couldn't create window: %s\n", SDL_GetError());
  }
}

internal
void
engine_init_context(engine_context *context) {
  context->base_path = SDL_GetBasePath();
  SLIST_INIT(&context->textures)
  c = context;
}

internal
void engine_init_lua(engine_context *c ){
  char b [1024];
  snprintf(b, 1024, "%sAssets/Scripts/main.lua", c->base_path);
  luaL_openlibs(c->g->L);
  luaL_loadfile(c->g->L, b);

  //This is required before calling any methods.
  lua_pcall(c->g->L, 0, 0, 0);
  api_init(c->g->L);

  lua_getglobal(c->g->L, "Init");
  lua_pushstring(c->g->L, c->base_path);
  lua_pcall(c->g->L,1,0,0);
}

internal
void
engine_init_game(engine_context *context) {
  context->g = GameInit();
  engine_init_lua(context);
}

internal
void
engine_halt_game(engine_context *context) {
  GameHalt(context->g);
}


int32_t
EngineStart(int32_t argc, char** argv)
{
  if( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) != 0)
  {
    fprintf(stderr, "\nUnable to initialize SDL: %s\n", SDL_GetError());
    return(1);
  }
  atexit(SDL_Quit);

  engine_context context = {0};
  engine_init_context(&context);
  engine_create_window(&context);
  engine_init_game(&context);
  engine_main_loop(&context);
  engine_halt_game(&context);
  SDL_DestroyWindow(context.window);

  return(0);
}

struct texture*
engine_load_sprite(const char* name){
  if(c && c->renderer){
    char *b  = (char*)malloc(1024);
    snprintf(b, 1024, "%sAssets/Sprites/%s.png", c->base_path, name);
    SDL_Texture *t = IMG_LoadTexture(c->renderer, b);
    if(t){
      struct texture *tex = (struct texture*)malloc(sizeof(struct texture));
      if(t && tex){
        tex->t = t;
        SLIST_INSERT_HEAD(&c->textures, tex, textures);
      }
      free(b);
      return tex;

    }
    free(b);
    return NULL;
  }
}
