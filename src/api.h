#ifndef __API_H
#define __API_H


extern "C"{
#include <lua.h>
};


#define LUA_FUNCTION(name) int name(lua_State *L)
typedef LUA_FUNCTION(lua_function);

void api_init(lua_State *L);

#endif
