#ifndef __ENGINE_H
#define __ENGINE_H
#include <inttypes.h>
#include <SDL_render.h>
#include <queue.h>
#include <game.h>


#ifndef NULL
#define NULL 0
#endif

#define internal static
#define global static
#define local static

#define PI32 3.141592653589793238462643383279502884197

typedef float float32_t;
typedef double float64_t;

struct engine_context
{
    char* base_path;
    SDL_Window *window;
    SDL_Renderer *renderer;

    int32_t window_width;
    int32_t window_height;

    struct game_context *g;

    SLIST_HEAD(,texture) textures;
};

struct engine_offscreen_buffer
{
  int32_t width;
  int32_t height;
  int32_t pitch;
  uint8_t bytes_per_pixel;
};

struct texture{
    SDL_Texture *t;
    SLIST_ENTRY(texture) textures;
};

struct texture* engine_load_sprite(const char* name);

int32_t
EngineStart(int32_t argc, char** argv);




#endif

