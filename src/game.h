#ifndef __GAME_H
#define __GAME_H

#include <SDL_surface.h>

extern "C"{
#include <lua.h>
#include <engine.h>
};


struct game_context{
    lua_State *L;
};

struct game_context * GameInit();
void GameHalt(game_context* g);
void GameUpdateAndRender(struct engine_context *c);

void*
load_sprite(const char* name);

#endif
