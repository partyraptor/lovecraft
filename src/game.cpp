#include <engine.h>

extern "C"{
#include <lauxlib.h>
}


static void stackDump (lua_State *L) {
  int i;
  int top = lua_gettop(L);
  for (i = 1; i <= top; i++) {  /* repeat for each level */
    int t = lua_type(L, i);
    switch (t) {

      case LUA_TSTRING:  /* strings */
        printf("`%s'", lua_tostring(L, i));
            break;

      case LUA_TBOOLEAN:  /* booleans */
        printf(lua_toboolean(L, i) ? "true" : "false");
            break;

      case LUA_TNUMBER:  /* numbers */
        printf("%g", lua_tonumber(L, i));
            break;

      default:  /* other values */
        printf("%s", lua_typename(L, t));
            break;

    }
    printf("  ");  /* put a separator */
  }
  printf("\n");  /* end the listing */
}

void GameUpdateAndRender(struct engine_context *c){
  struct game_context *g = c->g;
  if(g){
    lua_getglobal(g->L, "Update");
    lua_pcall(g->L,0,1,0);
    //stackDump(g->L);


    /* assuming the layers table is at stack position 1 */
    for (lua_pushnil(g->L); lua_next(g->L, 1); lua_pop(g->L, 1)) {
      /*                                  ^
       * remember to pop the value at the end of each iteration */
      /* key (index in the layers table of a sprites table) is at -2 and
       * value (a sprites table) is at -1 */
      for (lua_pushnil(g->L); lua_next(g->L, -2); lua_pop(g->L, 9)) {
        /*                                   ^
         * pop the value and the fields fetched below */
        /* key (index of a sprite table) is at -2 and value (a sprite
         * table) is at -1 */
        lua_getfield(g->L, -1, "name");
        lua_getfield(g->L, -2, "x");
        lua_getfield(g->L, -3, "y");
        lua_getfield(g->L, -4, "scalex");
        lua_getfield(g->L, -5, "scaley");
        lua_getfield(g->L, -6, "texture");
        lua_getfield(g->L, -7, "OnStart");
        lua_getfield(g->L, -8, "OnUpdate");
        void* dp = lua_touserdata(g->L, -3);
        struct texture* d = (struct texture*)dp;

        printf("%s: x=%d, y=%d, w=%f, h=%f\n", lua_tostring(g->L, -8), (int) lua_tointeger(g->L, -7),
               (int) lua_tointeger(g->L, -6), (float) lua_tonumber(g->L, -5), (float) lua_tonumber(g->L, -4));



        if(d) {
          SDL_Rect dstrect;
          dstrect.x = (int) lua_tointeger(g->L, -7);
          dstrect.y = (int) lua_tointeger(g->L, -6);
          SDL_QueryTexture(d->t, NULL, NULL, &dstrect.w, &dstrect.h);

          dstrect.w = (int) (float)dstrect.w * (float)lua_tonumber(g->L, -5);
          dstrect.h = (int) (float)dstrect.h * (float)lua_tonumber(g->L, -4);

          SDL_RenderCopy(c->renderer,d->t, NULL, &dstrect);
        }

      }
    }
  }
}


struct game_context * GameInit() {
  struct game_context *g = (struct game_context*) malloc(sizeof(struct game_context));
  if(g){
    g->L = luaL_newstate();
    return g;
  }
  return NULL;
}

void GameHalt(game_context* g) {
  if(g && g->L){
    lua_close(g->L);
    free(g);
  } else if(g){
    free(g);
  }
}

void* load_sprite(const char* s){
    return (void*)engine_load_sprite(s);
};
